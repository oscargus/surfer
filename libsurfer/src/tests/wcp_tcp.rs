use crate::message::Message;
use crate::wcp::proto::WcpSCMessage;
use crate::State;

use serde_json::Error as serde_Error;
use test_log::test;
use tokio::net::{TcpListener, TcpStream};
use tokio::time::{sleep, timeout, Duration};

use lazy_static::lazy_static;
use std::future::Future;
use std::sync::atomic::Ordering;
use std::sync::{Arc, Mutex};

fn get_test_port() -> usize {
    lazy_static! {
        static ref PORT_NUM: Arc<Mutex<usize>> = Arc::new(Mutex::new(54321));
    }
    let mut port = PORT_NUM.lock().unwrap();
    *port += 1;
    *port
}

async fn get_json_response(stream: &TcpStream) -> Result<WcpSCMessage, serde_Error> {
    stream.readable().await.expect("Failed to be readable");
    let mut data = vec![0; 1024];
    match stream.try_read(&mut data) {
        Ok(size) => {
            let cmd: Result<WcpSCMessage, _> = serde_json::from_slice(&data[..size - 1]);
            assert_eq!(data[size - 1], 0);
            cmd
        }
        Err(e) => panic!("Read failure: {e}"),
    }
}

async fn connect(port: usize) -> TcpStream {
    loop {
        if let Ok(c) = TcpStream::connect(format!("127.0.0.1:{port}")).await {
            return c;
        }
        sleep(Duration::from_millis(100)).await;
    }
}

async fn expect_disconnect(stream: &TcpStream) {
    loop {
        let mut buf = [0; 1024];
        stream.readable().await.expect("Stream was not readable");
        match stream.try_read(&mut buf) {
            Ok(0) => break,
            Ok(_) => continue,
            Err(_) => break,
        }
    }
}

fn run_test<F>(body: F)
where
    F: Future<Output = ()>,
{
    let runtime = tokio::runtime::Builder::new_current_thread()
        .worker_threads(2)
        .enable_all()
        .build()
        .unwrap();

    runtime.block_on(async {
        if let Err(_) = timeout(Duration::from_secs(30), body).await {
            panic!("Test timed out");
        }
    });
}

#[test]
fn stop_and_reconnect() {
    run_test(async {
        let mut state = State::new_default_config().unwrap();
        let port = get_test_port();
        for _ in 0..2 {
            state.update(Message::StartWcpServer {
                address: Some(format!("127.0.0.1:{port}").to_string()),
                initiate: false,
            });
            let stream = connect(port).await;
            get_json_response(&stream)
                .await
                .expect("failed to get WCP greeting");
            state.update(Message::StopWcpServer);
            expect_disconnect(&stream).await;
            loop {
                if !state.sys.wcp_running_signal.load(Ordering::Relaxed) {
                    break;
                }
                sleep(Duration::from_millis(100)).await;
            }
        }
    });
}

#[test]
fn reconnect() {
    run_test(async {
        let mut state = State::new_default_config().unwrap();
        let port = get_test_port();
        state.update(Message::StartWcpServer {
            address: Some(format!("127.0.0.1:{port}").to_string()),
            initiate: false,
        });
        for _ in 0..2 {
            let stream = connect(port).await;
            get_json_response(&stream)
                .await
                .expect("failed to get WCP greeting");
        }
    });
}

#[test]
fn initiate() {
    run_test(async {
        let mut state = State::new_default_config().unwrap();
        let port = get_test_port();
        let address = format!("127.0.0.1:{port}").to_string();
        let listener = TcpListener::bind(address.clone()).await.unwrap();
        state.update(Message::StartWcpServer {
            address: Some(address),
            initiate: true,
        });
        if let Ok((stream, _addr)) = listener.accept().await {
            get_json_response(&stream)
                .await
                .expect("failed to get WCP greeting");
        } else {
            panic!("Failed to connect");
        }
    });
}

async fn is_connected(stream: &TcpStream) -> bool {
    let mut buf = [0; 1];
    let result = stream.try_read(&mut buf);

    match result {
        Ok(0) => false, // Connection closed (EOF)
        Ok(_) => true,  // Data available
        Err(ref e) if e.kind() == std::io::ErrorKind::WouldBlock => true, // No data but still connected
        Err(_) => false, // Other error, likely disconnected
    }
}

#[test]
#[ignore = "This test is long running and disabled by default"]
fn long_pause() {
    run_test(async {
        let mut state = State::new_default_config().unwrap();
        let port = get_test_port();
        state.update(Message::StartWcpServer {
            address: Some(format!("127.0.0.1:{port}").to_string()),
            initiate: false,
        });
        let stream = connect(port).await;
        get_json_response(&stream)
            .await
            .expect("failed to get WCP greeting");

        // confirm that we can be silent for a while and still be connected
        std::thread::sleep(Duration::from_secs(10));
        if !is_connected(&stream).await {
            panic!("No longer connected");
        }
    });
}

#[test]
fn start_stop() {
    run_test(async {
        let mut state = State::new_default_config().unwrap();
        let port = get_test_port();
        state.update(Message::StartWcpServer {
            address: Some(format!("127.0.0.1:{port}").to_string()),
            initiate: false,
        });
        tokio::time::sleep(Duration::from_millis(1000)).await;
        state.update(Message::StopWcpServer);
        tokio::time::sleep(Duration::from_millis(1000)).await;
        if let Ok(_) = TcpStream::connect(format!("127.0.0.1:{port}")).await {
            panic!("Connected after stopping server");
        }
    });
}
